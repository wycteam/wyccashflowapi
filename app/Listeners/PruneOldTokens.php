<?php
namespace App\Listeners;

// use App\Events\Laravel\Passport\Events\AccessTokenCreated;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Events\RefreshTokenCreated;

class PruneOldTokens
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AccessTokenCreated $event
     * @return void
     */
    public function handle(RefreshTokenCreated $event)
    {
        //Log::info('event_refresh_accesstoken' . json_encode($event));
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id','<>',$event->accessTokenId)
            ->where('revoked', 0)
            ->delete();
    }
}
