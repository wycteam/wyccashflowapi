<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\MobilePush;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller{
    public function getAllUserInfo(){
        if(Auth::user()['identity']=='administrator'){
            $user=User::where('id','!=',Auth::user()->id)->get();
            return $this->jsonReturn(200,'Get all user information successfully/成功获取所有用户信息',$user);
        }elseif(Auth::user()['identity']=='admin'){
            return $this->jsonReturn(401,'Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }elseif(Auth::user()['identity']=='user'){
            $user=User::where('id','!=',Auth::user()->id)->get();
            return $this->jsonReturn(200,'Get all user information successfully/成功获取所有用户信息',$user);
        }
    }
    public function getCurrentUser(){
        if($user=Auth::user()){
            $data=$this->identity($user->identity);
            foreach($user->toArray() as $k=>$v){
                $data[$k]=$v;
            }
            $mobilePush=MobilePush::where('user_id',$user->id)->first();
            if($mobilePush){
                $data['device_type']=$mobilePush->device_type;
                $data['device_id']=$mobilePush->device_token;
            }else{
                $data['device_type']='';
                $data['device_id']='';
            }
            $data['default_password']=$user->defaulttag;
            $data['contact_number']=$user->number;
            $data['initial_rate']=$user->rate;
            $data['nick_name']=$user->name;
            return $this->jsonReturn(200,'Get the current user success/获取当前用户信息成功',$data);
        }else{
            return $this->jsonReturn(402,'Failed to get current user/无法获取当前用户信息','Failed to get current user/无法获取当前用户信息','Error');
        }
    }
    public function modifyCurrentUserInformation(Request $request){
        if(Auth::user()['identity']=='administrator'){
            $validator=Validator::make($request->all(),[
                'id'        =>'required',
            ]);
            if($validator->fails()) {
                return response()->json(['error'=>$validator->errors()]);
            }
            if(User::where('id',$request->id)
                ->update([
                    'name'=>$request->name,
                    'number'=>$request->number,
                    'email'=>$request->email,
                    'rate'=>$request->rate,
                ])){
                return $this->jsonReturn('200','Successfully modified/修改成功','Successfully modified/修改成功');
            };
        }else{
            return $this->jsonReturn('401','Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }
    }
    public function editPersonalInformation(Request $request){
        $validator=Validator::make($request->all(),[
            'currentPassword'   =>'required',
            'newPassword'       =>'required',
            'confirmNewPassword'=>'required|same:newPassword'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()]);
        }
        if($request->newPassword!=$request->confirmNewPassword){
            return $this->jsonReturn('402','wrong password/密码错误','wrong password/密码错误','Error');
        }
        if($request->currentPassword&&!empty($request->nickname)&&!empty($request->email)&&!empty($request->number)){
            if(Hash::check($request->currentPassword,Auth::user()->password)){
                User::where('id',Auth::user()->id)
                    ->update([
                        'name'=>$request->nickname,
                        'email'=>$request->email,
                        'password'=>bcrypt($request->newPassword),
                        'number'=>$request->number,
                        'img'=>$request->img,
                        'defaulttag'=>"0",
                    ]);
                return $this->jsonReturn('200','Successfully modified/修改成功','Successfully modified/修改成功');
            }else{
                return $this->jsonReturn('402','wrong password/密码错误','wrong password/密码错误','Error');
            }
        }else{
            if(Hash::check($request->currentPassword,Auth::user()->password)){
                User::where('id',Auth::user()->id)
                    ->update([
                        'password'=>bcrypt($request->newPassword),
                        'defaulttag'=>"0",
                    ]);
                return $this->jsonReturn('200','Successfully modified/修改成功','Successfully modified/修改成功');
            }else{
                return $this->jsonReturn('402','wrong password/密码错误','wrong password/密码错误','Error');
            }
        }
    }
}
