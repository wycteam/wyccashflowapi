<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MobilePush;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller{
    public function login(Request $request){
        if(Auth::attempt(['email'=>$request->emailOrContactNumber,'password'=>$request->password])||Auth::attempt(['number'=>$request->emailOrContactNumber,'password'=>$request->password])){
            $user=Auth::user();
            $http=new Client;
            $response = $http->post(config('api.domain').'/oauth/token', [
                'form_params' => [
                    'grant_type' =>config('api.grant_type'),
                    'client_id' =>config('api.client_id'),
                    'client_secret' =>config('api.client_secret'),
                    'username' =>$request->emailOrContactNumber,
                    'password' =>$request->password,
                    'scope' =>config('api.scope'),//授权应用程序支持的所有范围的令牌
                ],
            ]);
            $data=$this->identity($user->identity);
            $data['id']=$user->id;
            if(!empty($request->deviceType&&!empty($request->deviceToken))){
                DB::beginTransaction();
                try{
                    DB::table('mobile_pushes')
                        ->updateOrInsert([
                            'user_id'=>$user->id
                        ],[
                            'user_id'=>$user->id,
                            'device_type'=>$request->deviceType,
                            'device_token'=>$request->deviceToken
                        ]);
                    $data['device_type']=$request->deviceType;
                    $data['device_id']=$request->deviceToken;
                    DB::commit();
                }catch(\Exception $e){
                    DB::rollBack();
                }
            }
            $data['email']=$user->email;
            $data['nick_name']=$user->name;
            $data['contact_number']=$user->number;
            $data['identity']=$user->identity;
            $data['balance']=$user->balance;
            $data['initial_rate']=$user->rate;
            $data['default_password']=$user->defaulttag;
            $data['token']=json_decode( (string) $response->getBody());
            $data['token']=$data['token']->access_token;
            $data['code']=200;
            $data['msg']='User login succeeded/用户登录成功';
            $data['data']=$data;
            $data['status']='Success';
            return response()->json($data);
        }else{
            return $this->jsonReturn(402,'wrong password/密码错误','wrong password/密码错误','Error');
        }
    }
    public function register(Request $request){
        if(Auth::user()['identity']=='administrator'){
            $validator =Validator::make($request->all(),[
                'nickName'       =>'required',
                'email'          =>'required|email|unique:users',
                'number'         =>'required',
            ]);
            if($validator->fails()) {
                return response()->json(['error'=>$validator->errors()]);
            }
            if($request->identity=='administrator'){
                return $this->jsonReturn('401','Insufficient permissions/权限不足','Insufficient permissions/权限不足');
            }
            DB::beginTransaction();
            try{
                $id=DB::table('users')->insertGetId([
                    "name"=>$request->nickName,
                    "password"=>bcrypt($request->number),
                    "number"=>$request->number,
                    "email"=>$request->email,
                    "identity"=>$request->identity,
                    "rate"=>$request->rate,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                ]);
                DB::table('mobile_pushes')->insert([
                    'user_id'=>$id,
                    'device_type'=>'N/A',
                    'device_token'=>'',
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                ]);
                DB::commit();
                return $this->jsonReturn('200','User registration is successful/用户注册成功','User registration is successful/用户注册成功');
            }catch(\Exception $e){
                DB::rollBack();
                return $this->jsonReturn('402','User registration failed/用户注册失败','User registration failed/用户注册失败','Error');
            }
        }else{
            return $this->jsonReturn('401','Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }
    }
    public function logout(){
        if(Auth::user()){
            DB::table('oauth_access_tokens')
                ->where('user_id',Auth::user()->id)
                ->delete();
            return $this->jsonReturn('200','User exited successfully/用户退出成功','User exited successfully/用户退出成功');
        }else{
            return $this->jsonReturn('402','User exit failed/用户退出失败','User exit failed/用户退出失败','Error');
        }
    }
}
