<?php

namespace App\Http\Controllers\Api;

use App\Models\Cashflow;
use App\Http\Controllers\Controller;
use App\Models\MobilePush;
use App\Models\ServiceFee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;

class CashflowController extends Controller
{
    //
    public function number(Request $request){
        if($user=Auth::user()){
            $user=User::where('number',$request->number)->first();
            if(!empty($user)){
                $user['contact_number']=$user->number;
                $user['nick_name']=$user->name;
                return $this->jsonReturn('200','Get success/获取成功',$user);
            }else{
                $data['code']="402";
                $data['msg']='No such person found/查无此人';
                $data['message']='No such person found/查无此人';
                $data['status']='Error';
                return response()->json($data);
            }
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function checkpendingtransaction(){
        if($user=Auth::user()){
            $cashflow=Cashflow::where('user_id',$user->id)->orderby('id','desc')->first();
            $recipientdata=Cashflow::where('user_id',$cashflow->recipientid)->orderby('id','desc')->first();
            if($recipientdata->status!=0){
                $data['id']=$recipientdata->id;
                $data['action']=$recipientdata->action;
                if($recipientdata->currencies=="cn"){
                    $data['currency_id']=1;
                }elseif($recipientdata->currencies=="ph"){
                    $data['currency_id']=2;
                }elseif($recipientdata->currencies=="us"){
                    $data['currency_id']=3;
                }
                $data['amount']=$recipientdata->amount;
                $data['user_id']=$recipientdata->user_id;
                $data['sender_id']=$user->id;
                $data['recipient_id']=$recipientdata->user_id;
                $data['created_at']=(string)$recipientdata->created_at;
                $data['updated_at']=(string)$recipientdata->updated_at;
                return $this->jsonReturn('200','Get data success/获得数据成功',$data);
            }else{
                return $this->jsonReturn('402','Pending transaction/待处理的交易','Pending transaction/待处理的交易','Error');
            }
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function transfer(Request $request){
        if(Auth::user()){
            $user=User::where('id',Auth::user()->id)->first();
            $user1=User::where('id',$request->recipientId)->first();
            if(empty($user1)){
                $user1=$user;
            }
            $admin=User::where('identity','administrator')->first();
            if($request->currencyId==1){
                $currencies='cn';
                $chinesel='人民币';
                $balance=$user->cn;
                $balance1=$user1->cn;
                $balanceAdmin=$admin->cn;
            }elseif($request->currencyId==2){
                $currencies='ph';
                $chinesel='比索';
                $balance=$user->ph;
                $balance1=$user1->ph;
                $balanceAdmin=$admin->ph;
            }elseif($request->currencyId==3){
                $currencies='us';
                $chinesel='美金';
                $balance=$user->us;
                $balance1=$user1->us;
                $balanceAdmin=$admin->us;
            }
            if($balance<$request->amount){
                return $this->jsonReturn('402','Insufficient balance/余额不足','Insufficient balance/余额不足','Error');
            }
            $mobilePush=MobilePush::where('user_id',Auth::user()->id)->first();
            $mobilePush1=MobilePush::where('user_id',$request->recipientId)->first();
            $sources=$user->name.'转账给用户'.$user1->name.'---金额'.$request->amount.$currencies.'---'.$user1->name.'余额增加'.$request->amount.$currencies;
            DB::beginTransaction();
            try{
                $id=DB::table('cashflows')->insertGetId([
                    'user_id'      =>$request->recipientId,
                    'action'       =>$request->action,
                    'transfer_role'=>'recipient',
                    'currencies'   =>$currencies,
                    'amount'       =>$request->amount,
                    'rate'         =>0,
                    'profit'       =>0,
                    'total_balance'=>$balanceAdmin,
                    'source'       =>$sources,
                    'balance'      =>$balance1+$request->amount,
                    'nickname'     =>$user1->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$user1->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                    'status'       =>1,
                ]);
                DB::table('cashflows')->insert([
                    'user_id'      =>Auth::user()->id,
                    'action'       =>$request->action,
                    'transfer_role'=>'sender',
                    'currencies'   =>$currencies,
                    'amount'       =>-$request->amount,
                    'rate'         =>0,
                    'profit'       =>0,
                    'total_balance'=>$balanceAdmin-$request->amount,
                    'source'       =>$sources,
                    'balance'      =>$balance-$request->amount,
                    'nickname'     =>$user->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$user->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                    'recipientid'  =>$request->recipientId,
                    'status'       =>1,
                ]);
                DB::table('users')->where('id','=',Auth::user()->id)->update([
                    $currencies=>$balance-$request->amount,
                ]);
                DB::table('users')->where('id','=',$request->recipientId)->update([
                    $currencies=>$balance1+$request->amount,
                ]);
                DB::commit();
                if(!empty($mobilePush)){
                    $this->push('转账消息','您的账户于'.date('Y-m-d H:i:s').'转账给用户：'.$user1->name.$request->amount.$chinesel."\n".'您的账户余额为'.($balance-$request->amount).$chinesel,$mobilePush->device_token);
                }elseif(!empty($mobilePush1)){
                    $this->push('转账消息',$user->name.'于'.date('Y-m-d H:i:s').'转账给你：'.$request->amount.$chinesel."\n".'您的账户余额为'.($balance1+$request->amount).$chinesel,$mobilePush1->device_token);
                }
                $data['action']=$request->action;
                $data['currency_id']=$request->currencyId;
                $data['amount']=$request->amount;
                $data['sender_id']=Auth::user()->id;
                $data['recipient_id']=$request->recipientId;
                $data['updated_at']=date("Y-m-d H:i:s",time());
                $data['created_at']=date("Y-m-d H:i:s",time());
                $data['id']=$id;
                return $this->jsonReturn('200','Successful transfer/转账成功',$data);
            }catch(\Exception $e){
                DB::rollBack();
                return $this->jsonReturn('402','Transfer failed/转账失败','Transfer failed/转账失败','Error');
            }
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function verifypassword(Request $request){
        if(Auth::user()){
            if(Hash::check($request->password,Auth::user()->password)){
                $cashflow=Cashflow::where('id',$request->transactionId)->first();
                if($cashflow->currencies=="cn"){
                    $currency_id="1";
                }elseif($cashflow->currencies=="ph"){
                    $currency_id="2";
                }elseif($cashflow->currencies=="us"){
                    $currency_id="3";
                }
                $data['currency_id']=$currency_id;
                $data['currency_name']=$cashflow->currencies;
                $data['balance']=$cashflow->balance;
                return $this->jsonReturn('200','Get success/获取成功',$data);
            }else{
                return $this->jsonReturn('402','wrong password/密码错误','wrong password/密码错误','Error');
            }
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function currencies(){
            $data=[];
            for($i=1;$i<=3;$i++){
                if($i==1){
                    $v['id']=$i;
                    $v['name']="CN";
                    $data[]=$v;
                }elseif($i==2){
                    $v['id']=$i;
                    $v['name']="PH";
                    $data[]=$v;
                }elseif($i==3){
                    $v['id']=$i;
                    $v['name']="US";
                    $data[]=$v;
                }
            }
            return $this->jsonReturn('200','Get currency type success/获得货币类型成功',$data);
    }
    public function summaries(){
        if(Auth::user()){
            $indata=[];
            for ($i=1;$i<=3;$i++) {
                if ($i == 1) {
                    $v['id'] = $i;
                    $v['name'] = "CN";
                    $currency['currency'] = $v;
                    $currency['total_available_balance'] = Auth::user()->cn;
                    if (Auth::user()->cnprofit==null){
                        $currency['total_service_fee'] ="0.00";
                    }else{
                        $currency['total_service_fee'] = Auth::user()->cnprofit;
                    }
                    $indata[]=$currency;
                } elseif ($i == 2) {
                    $v['id'] = $i;
                    $v['name'] = "PH";
                    $currency['currency'] = $v;
                    $currency['total_available_balance'] = Auth::user()->ph;
                    if (Auth::user()->phprofit==null){
                        $currency['total_service_fee'] = "0.00";
                    }else{
                        $currency['total_service_fee'] = Auth::user()->phprofit;
                    }
                    $indata[] = $currency;
                } elseif ($i == 3) {
                    $v['id'] = $i;
                    $v['name'] = "US";
                    $currency['currency'] = $v;
                    $currency['total_available_balance'] = Auth::user()->us;
                    if (Auth::user()->usprofit==null){
                        $currency['total_service_fee'] = "0.00";
                    }else{
                        $currency['total_service_fee'] = Auth::user()->usprofit;
                    }
                    $indata[] =$currency;
                }
            }
            $cashflowfirst=Cashflow::orderby('id','desc')->first();
            $user=User::where('id',$cashflowfirst->user_id)->first();
            $a=[];
            for($i=0;$i<1;$i++){
                $a['id']=$cashflowfirst->id;
                $a['user_id']=$cashflowfirst->user_id;
                $a['action']=$cashflowfirst->action;
                $a['amount']=$cashflowfirst->amount;
                $a['balance']=$cashflowfirst->balance;
                $a['rate']=$cashflowfirst->rate;
                $a['proﬁt']=$cashflowfirst->profit;
                $a['source']=$cashflowfirst->source;
                $a['date_recorded']=date('Y-m-d', strtotime($cashflowfirst->created_at));
                $a['user_name']=$user->name;
                $a['operator']=$cashflowfirst->operatorid;
                $a['operator_name']=$cashflowfirst->operator;
            }
            if($cashflowfirst->currencies=="cn"){
                $currency_id="1";
            }elseif($cashflowfirst->currencies=="ph"){
                $currency_id="2";
            }elseif($cashflowfirst->currencies=="us"){
                $currency_id="3";
            }
            $a['currency_id']=$currency_id;
            $a['total_user_balance']=$cashflowfirst->balance;
            $a['transfer_role']=$cashflowfirst->transfer_role;
            $data['latest_transaction']=[];
            $data['latest_transaction'][]=$a;
            $data['code']=200;
            $data['msg']='Get success/获取成功';
            $data['data']=$indata;
            $data['status']='Success';
            return response()->json($data);
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function getCurrentUserTransactionData(Request $request){
        if($request->id==1){
            $currency="cn";
        }elseif($request->id==2){
            $currency="ph";
        }elseif($request->id==3){
            $currency="us";
        }
        if(Auth::user()->identity=='administrator'){
            $data=Cashflow::where('currencies',$currency)->orderby('id','desc')->get();
            if(count($data)!=0){
                $user=User::where('id',$data[0]->user_id)->first();
                foreach($data as $v){
                    $v['date']=date('Y-m-d', strtotime($v['created_at']));
                    $v['email']=$user->email;
                    $v['contact_number']=$v['number'];
                    $v['service_fee']=$v['profit'];
                    if($v->transfer_role==null){
                        $v['transfer_role']='';
                    }
                    $v['source']=null;
                    if($v['action']=="transfer"){
                        $v['action']="转账";
                    }elseif($v['action']=="deposit"){
                        $v['action']="存款";
                    }elseif($v['action']=="withdraw"){
                        $v['action']="取款";
                    }elseif($v['action']=="withdraw service fee"){
                        $v['action']="提取手续费";
                    }
                }
                return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data,'Success',$data[0]->total_balance);
            }else{
                return $this->jsonReturn('402','no data/暂无数据','no data/暂无数据','Error');
            }
        }elseif(Auth::user()->identity=='admin'){
            $data=Cashflow::where('currencies',$currency)->orderby('id','desc')->get(['id','user_id','action','transfer_role','currencies','amount','rate','profit','total_balance','created_at','balance','operator']);
            if(count($data)!=0){
                $user=User::where('id',$data[0]->user_id)->first();
                foreach($data as $v){
                    $v['date']=date('Y-m-d', strtotime($v['created_at']));
                    $v['email']=$user->email;
                    $v['contact_number']=$v['number'];
                    $v['service_fee']=$v['profit'];
                    if($v->transfer_role==null){
                        $v['transfer_role']='';
                    }
                    $v['source']=null;
                    if($v['action']=="transfer"){
                        $v['action']="转账";
                    }elseif($v['action']=="deposit"){
                        $v['action']="存款";
                    }elseif($v['action']=="withdraw"){
                        $v['action']="取款";
                    }elseif($v['action']=="withdraw service fee"){
                        $v['action']="提取手续费";
                    }
                }
                return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data);
            }else{
                return $this->jsonReturn('402','no data/暂无数据','no data/暂无数据','Error');
            }
        }elseif(Auth::user()->identity=='user'){
            $data=Cashflow::where('currencies',$currency)->where('user_id',Auth::user()->id)->orderBy('id','desc')->get(['id','user_id','action','transfer_role','currencies','amount','rate','profit','source','created_at','updated_at','balance','nickname','operator','number']);
            if(count($data)!=0){
                $user=User::where('id',$data[0]->user_id)->first();
                foreach($data as $v){
                    $v['date']=date('Y-m-d', strtotime($v['created_at']));
                    $v['email']=$user->email;
                    $v['contact_number']=$v['number'];
                    $v['service_fee']=$v['profit'];
                    if($v->transfer_role==null){
                        $v['transfer_role']='';
                    }
                    $v['source']=null;
                    if($v['action']=="transfer"){
                        $v['action']="转账";
                    }elseif($v['action']=="deposit"){
                        $v['action']="存款";
                    }elseif($v['action']=="withdraw"){
                        $v['action']="取款";
                    }elseif($v['action']=="withdraw service fee"){
                        $v['action']="提取手续费";
                    }
                }
                return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data,'Success',$data[0]->balance);
            }else{
                return $this->jsonReturn('402','no data/暂无数据','no data/暂无数据','Error');
            }
        }else{
            return $this->jsonReturn('402','please sign in/请登录','please sign in/请登录','Error');
        }
    }
    public function getTheTransactionLog(Request $request){
        if(Auth::user()->identity=='administrator'){
            $data=Cashflow::where('currencies',$request->currency)->orderby('id','desc')->get();
            return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data,'Success',$data[0]->total_balance);
        }elseif(Auth::user()->identity=='admin'){
            $data=Cashflow::where('currencies',$request->currency)->orderby('id','desc')->get(['id','user_id','action','transfer_role','currencies','amount','rate','profit','total_balance','created_at','balance','operator']);
            return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data);
        }elseif(Auth::user()->identity=='user'){
            $data=Cashflow::where('currencies',$request->currency)->where('user_id',Auth::user()->id)->orderBy('id','desc')->get(['id','user_id','action','transfer_role','currencies','amount','rate','profit','source','created_at','updated_at','balance','nickname','operator','number']);
            return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$data,'Success',$data[0]->balance);
        }else{
            return $this->jsonReturn('401','Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }
    }
    public function getServiceFeeFlowLog(Request $request){
        if(Auth::user()->identity=='administrator'){
            $serviceFee=ServiceFee::where('currencies',$request->currency)->orderBy('id','desc')->get();
            return $this->jsonReturn(200,'Successfully obtained service fee log/成功获取服务费日志',$serviceFee);
        }elseif(Auth::user()->identity=='admin'){
            $serviceFee=ServiceFee::where('currencies',$request->currency)->orderby('id','desc')->get(['id','user_id','action','currencies','amount','rate','profit','total_ServiceFee','created_at','operator']);
            return $this->jsonReturn('200','Get The Transaction Log/获取交易日志',$serviceFee);
        }else{
            return $this->jsonReturn('401','Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }
    }
    public function addTheTransactionLog(Request $request){
        //radio = currency Translation error. Causes the field name to be incorrect. Sorry
        //radio = currency 翻译错误。导致字段名称不正确。抱歉
        if($request->identity=='admin'){
            return $this->jsonReturn(401,'Insufficient permissions/权限不足','Insufficient permissions/权限不足');
        }
        $validator=Validator::make($request->all(),[
            'id'        =>'required_if:action,deposit|required_if:action,withdraw',
            'action'    =>'required',
            'amount'    =>'required',
            'profit'    =>'required_if:action,deposit',
            'radio'     =>'required',
            'id1'       =>'required_if:action,transfer'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()]);
        }
        $sources=[];
        if($request->source!=''){
            foreach($request->source as $source){
                $sources[]=$source['value'];
            };
        }
        $sources=implode("---",$sources);
        if($request->action!='withdraw service fee'){
            $user=User::where('id',$request->id)->first();
            $user1=User::where('id',$request->id1)->first();
            if(empty($user1)){
                $user1=$user;
            }
            $admin=User::where('identity','administrator')->first();
            if($request->radio=='cn'){
                $currencies='cn';
                $chinesel='人民币';
                $currenciesprofit='cnprofit';
                $balance=$user->cn;
                $balance1=$user1->cn;
                $profit=$admin->cnprofit;
                $balanceAdmin=$admin->cn;
            }elseif($request->radio=='ph'){
                $currencies='ph';
                $chinesel='比索';
                $currenciesprofit='phprofit';
                $balance=$user->ph;
                $balance1=$user1->ph;
                $profit=$admin->phprofit;
                $balanceAdmin=$admin->ph;
            }elseif($request->radio=='us'){
                $currencies='us';
                $chinesel='美金';
                $currenciesprofit='usprofit';
                $balance=$user->us;
                $balance1=$user1->us;
                $profit=$admin->usprofit;
                $balanceAdmin=$admin->us;
            }
        }else{
            $admin=User::where('identity','administrator')->first();
            if($request->radio=='cn'){
                $currencies='cn';
                $currenciesprofit='cnprofit';
                $profit=$admin->cnprofit;
                $balanceAdmin=$admin->cn;
            }elseif($request->radio=='ph'){
                $currencies='ph';
                $currenciesprofit='phprofit';
                $profit=$admin->phprofit;
                $balanceAdmin=$admin->ph;
            }elseif($request->radio=='us'){
                $currencies='us';
                $currenciesprofit='usprofit';
                $profit=$admin->usprofit;
                $balanceAdmin=$admin->us;
            }
        }
        $mobilePush=MobilePush::where('user_id',$request->id)->first();
        if($request->action=='deposit'){
            DB::beginTransaction();
            try{
                DB::table('cashflows')->insert([
                    'user_id'      =>$request->id,
                    'action'       =>$request->action,
                    'currencies'   =>$request->radio,
                    'amount'       =>$request->amount,
                    'rate'         =>$request->rate,
                    'profit'       =>$request->profit,
                    'total_balance'=>$request->amount+$balanceAdmin,
                    'source'       =>$sources,
                    'balance'      =>$balance+$request->amount-$request->profit,
                    'nickname'     =>$user->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$user->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                    'status'       =>1,
                    'operatorid'   =>Auth::user()->id,
                ]);
                DB::table('service_fees')->insert([
                    'user_id'      =>$request->id,
                    'action'       =>$request->action,
                    'currencies'   =>$request->radio,
                    'amount'       =>$request->amount,
                    'rate'         =>$request->rate,
                    'profit'       =>$request->profit,
                    'total_ServiceFee'=>$profit+$request->profit,
                    'source'       =>$sources,
                    'nickname'     =>$user->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$user->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                ]);
                DB::table('users')->where('id','=',$request->id)->update([
                    $currencies=>$balance+$request->amount-$request->profit,
                ]);
                DB::table('users')->where('identity','=','administrator')->update([
                    $currenciesprofit=>$request->profit+$profit,
                    $currencies=>$balanceAdmin+$request->amount,
                ]);
                DB::commit();
                if(!empty($mobilePush)){
                    $this->push('现金存款','您的账户于'.date('Y-m-d H:i:s').'存款'.$request->amount.$chinesel."\n".'服务费'.$request->profit.$chinesel."\n".'您的账户余额为'.($balance+$request->amount-$request->profit).$chinesel,$mobilePush->device_token);
                }
                return $this->jsonReturn(200,'Deposit successful/存款成功','Deposit successful/存款成功');
            }catch(\Exception $e){
                DB::rollBack();
                return $this->jsonReturn(402,'Deposit failed/存款失败','Deposit failed/存款失败','Error');
            }
        }elseif($request->action=='withdraw'){
            if($balance<$request->amount){
                return $this->jsonReturn('402','Insufficient amount/金额不足','Insufficient amount/金额不足','Error');
            }
            DB::beginTransaction();
            try{
                DB::table('cashflows')->insert([
                    'user_id'      =>$request->id,
                    'action'       =>$request->action,
                    'currencies'   =>$request->radio,
                    'amount'       =>-$request->amount,
                    'rate'         =>0,
                    'profit'       =>0,
                    'total_balance'=>$balanceAdmin-$request->amount,
                    'source'       =>$sources,
                    'balance'      =>$balance-$request->amount,
                    'nickname'     =>$user->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$user->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                    'status'       =>1,
                    'operatorid'   =>Auth::user()->id,
                ]);
                DB::table('users')->where('id','=',$request->id)->update([
                    $currencies=>$balance-$request->amount,
                ]);
                DB::table('users')->where('identity','=','administrator')->update([
                    $currencies=>$balanceAdmin-$request->amount,
                ]);
                DB::commit();
                if(!empty($mobilePush)){
                    $this->push('取款消息','您的账户于'.date('Y-m-d H:i:s').'取款'.$request->amount.$chinesel."\n".'您的账户余额为'.($balance-$request->amount).$chinesel,$mobilePush->device_token);
                }
                return $this->jsonReturn(200,'Withdrawal successful/取款成功','Withdrawal successful/取款成功');
            }catch(\Exception $e){
                DB::rollBack();
                return $this->jsonReturn(402,'Withdrawal failed/取款失败','Withdrawal failed/取款失败','Error');
            }
        }elseif($request->action=='withdraw service fee'){
            if($profit<$request->amount){
                return $this->jsonReturn('402','Insufficient service fee/服务费不足','Insufficient service fee/服务费不足','Error');
            }
            DB::beginTransaction();
            try{
                DB::table('cashflows')->insert([
                    'user_id'      =>$admin->id,
                    'action'       =>$request->action,
                    'currencies'   =>$request->radio,
                    'amount'       =>-$request->amount,
                    'rate'         =>0,
                    'profit'       =>-$request->amount,
                    'total_balance'=>$balanceAdmin-$request->amount,
                    'source'       =>$sources,
                    'balance'      =>$profit-$request->amount,
                    'nickname'     =>$admin->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>$admin->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                    'status'       =>1,
                    'operatorid'   =>Auth::user()->id,
                ]);
                DB::table('service_fees')->insert([
                    'user_id'      =>$admin->id,
                    'action'       =>$request->action,
                    'currencies'   =>$request->radio,
                    'amount'       =>-$request->amount,
                    'rate'         =>0,
                    'profit'       =>-$request->amount,
                    'total_ServiceFee'=>$profit-$request->amount,
                    'source'       =>$sources,
                    'nickname'     =>Auth::user()->name,
                    'operator'     =>Auth::user()->name,
                    'number'       =>Auth::user()->number,
                    'created_at'   =>date("Y-m-d H:i:s",time()),
                    'updated_at'   =>date("Y-m-d H:i:s",time()),
                ]);
                DB::table('users')->where('identity','=','administrator')->update([
                    $currenciesprofit=>$profit-$request->amount,
                    $currencies=>$balanceAdmin-$request->amount,
                ]);
                DB::commit();
                return $this->jsonReturn(200,'Successfully withdrawn fee/提取手续费成功','Successfully withdrawn fee/提取手续费成功');
            }catch(\Exception $e){
                DB::rollBack();
                return $this->jsonReturn(402,'Failed withdrawn fee/提取手续费失败','Failed withdrawn fee/提取手续费失败','Error');
            }
        }elseif($request->action=='transfer'){
            if($balance<$request->amount){
                return $this->jsonReturn('402','Insufficient balance/余额不足','Insufficient balance/余额不足','Error');
            }
            $mobilePush1=MobilePush::where('user_id',$request->id1)->first();
            $source=$user->name.'转账给用户'.$user1->name.'---金额'.$request->amount.$request->radio.'---'.$user1->name.'余额增加'.$request->amount.$request->radio;
            $sources=$source.'---'.$sources;
            if($request->pass){
                if(Hash::check($request->pass,Auth::user()->password)){
                    DB::beginTransaction();
                    try{
                        DB::table('cashflows')->insert([
                            'user_id'      =>$request->id1,
                            'action'       =>$request->action,
                            'transfer_role'=>'recipient',
                            'currencies'   =>$request->radio,
                            'amount'       =>$request->amount,
                            'rate'         =>0,
                            'profit'       =>0,
                            'total_balance'=>$balanceAdmin,
                            'source'       =>$sources,
                            'balance'      =>$balance1+$request->amount,
                            'nickname'     =>$user1->name,
                            'operator'     =>Auth::user()->name,
                            'number'       =>$user1->number,
                            'created_at'   =>date("Y-m-d H:i:s",time()),
                            'updated_at'   =>date("Y-m-d H:i:s",time()),
                            'status'       =>1,
                            'operatorid'   =>Auth::user()->id,
                        ]);
                        DB::table('cashflows')->insert([
                            'user_id'      =>$request->id,
                            'action'       =>$request->action,
                            'transfer_role'=>'sender',
                            'currencies'   =>$request->radio,
                            'amount'       =>-$request->amount,
                            'rate'         =>0,
                            'profit'       =>0,
                            'total_balance'=>$balanceAdmin-$request->amount,
                            'source'       =>$sources,
                            'balance'      =>$balance-$request->amount,
                            'nickname'     =>$user->name,
                            'operator'     =>Auth::user()->name,
                            'number'       =>$user->number,
                            'created_at'   =>date("Y-m-d H:i:s",time()),
                            'updated_at'   =>date("Y-m-d H:i:s",time()),
                            'status'       =>1,
                            'recipientid'  =>$request->id1,
                            'operatorid'   =>Auth::user()->id,
                        ]);
                        DB::table('users')->where('id','=',$request->id)->update([
                            $currencies=>$balance-$request->amount,
                        ]);
                        DB::table('users')->where('id','=',$request->id1)->update([
                            $currencies=>$balance1+$request->amount,
                        ]);
                        DB::commit();
                        if(!empty($mobilePush)){
                            $this->push('转账消息','您的账户于'.date('Y-m-d H:i:s').'转账给用户：'.$user1->name.$request->amount.$chinesel."\n".'您的账户余额为'.($balance-$request->amount).$chinesel,$mobilePush->device_token);
                        }elseif(!empty($mobilePush1)){
                            $this->push('转账消息',$user->name.'于'.date('Y-m-d H:i:s').'转账给你：'.$request->amount.$chinesel."\n".'您的账户余额为'.($balance1+$request->amount).$chinesel,$mobilePush1->device_token);
                        }
                        return $this->jsonReturn('200','Successful transfer/转账成功','Successful transfer/转账成功');
                    }catch(\Exception $e){
                        DB::rollBack();
                        return $this->jsonReturn('402','Transfer failed/转账失败','Transfer failed/转账失败','Error');
                    }
                }else{
                    return $this->jsonReturn('402','wrong password/密码错误','wrong password/密码错误','Error');
                }
            }else{
                DB::beginTransaction();
                try{
                    DB::table('cashflows')->insert([
                        'user_id'      =>$request->id1,
                        'action'       =>$request->action,
                        'transfer_role'=>'recipient',
                        'currencies'   =>$request->radio,
                        'amount'       =>$request->amount,
                        'rate'         =>0,
                        'profit'       =>0,
                        'total_balance'=>$balanceAdmin,
                        'source'       =>$sources,
                        'balance'      =>$balance1+$request->amount,
                        'nickname'     =>$user1->name,
                        'operator'     =>Auth::user()->name,
                        'number'       =>$user1->number,
                        'created_at'   =>date("Y-m-d H:i:s",time()),
                        'updated_at'   =>date("Y-m-d H:i:s",time()),
                        'status'       =>1,
                        'operatorid'   =>Auth::user()->id,
                    ]);
                    DB::table('cashflows')->insert([
                        'user_id'      =>$request->id,
                        'action'       =>$request->action,
                        'transfer_role'=>'sender',
                        'currencies'   =>$request->radio,
                        'amount'       =>-$request->amount,
                        'rate'         =>0,
                        'profit'       =>0,
                        'total_balance'=>$balanceAdmin-$request->amount,
                        'source'       =>$sources,
                        'balance'      =>$balance-$request->amount,
                        'nickname'     =>$user->name,
                        'operator'     =>Auth::user()->name,
                        'number'       =>$user->number,
                        'created_at'   =>date("Y-m-d H:i:s",time()),
                        'updated_at'   =>date("Y-m-d H:i:s",time()),
                        'status'       =>1,
                        'recipientid'  =>$request->id1,
                        'operatorid'   =>Auth::user()->id,
                    ]);
                    DB::table('users')->where('id','=',$request->id)->update([
                        $currencies=>$balance-$request->amount,
                    ]);
                    DB::table('users')->where('id','=',$request->id1)->update([
                        $currencies=>$balance1+$request->amount,
                    ]);
                    DB::commit();
                    if(!empty($mobilePush)){
                        $this->push('转账消息','您的账户于'.date('Y-m-d H:i:s').'转账给用户：'.$user1->name.$request->amount.$chinesel."\n".'您的账户余额为'.($balance-$request->amount).$chinesel,$mobilePush->device_token);
                    }elseif(!empty($mobilePush1)){
                        $this->push('转账消息',$user->name.'于'.date('Y-m-d H:i:s').'转账给你：'.$request->amount.$chinesel."\n".'您的账户余额为'.($balance1+$request->amount).$chinesel,$mobilePush1->device_token);
                    }
                    return $this->jsonReturn('200','Successful transfer/转账成功','Successful transfer/转账成功');
                }catch(\Exception $e){
                    DB::rollBack();
                    return $this->jsonReturn('402','Transfer failed/转账失败','Transfer failed/转账失败','Error');
                }
            }
        }
    }
}
