<?php

namespace App\Http\Controllers;

use Edujugon\PushNotification\PushNotification;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function jsonReturn($code,$msg,$value,$status="Success",$total='',$fee="0.00"){
        $data['code']=$code;
        $data['msg']=$msg;
        $data['data']=$value;
        $data['status']=$status;
        $data['total_available_balance']=$total;
        $data['total_service_fee']=$fee;
        return response()->json($data);
    }
    public function push($title,$body,$token){
        $push = new PushNotification('apn');
        $push->setMessage([
            'aps' => [
                'alert' => [
                    'title' =>$title,
                    'body' =>$body
                ],
                'sound' => 'default',
                'badge' => 1
            ]
        ])->setDevicesToken([$token])
            ->send()
            ->getFeedback();
    }
    public function identity($type){
        if($type=="administrator"){
            $data['is_super_admin']=true;
            $data['is_admin']=false;
            $data['is_user']=false;
            return $data;
        }elseif($type=="admin"){
            $data['is_super_admin']=false;
            $data['is_admin']=true;
            $data['is_user']=false;
            return $data;
        }elseif($type=="user"){
            $data['is_super_admin']=false;
            $data['is_admin']=false;
            $data['is_user']=true;
            return $data;
        }
    }
}
