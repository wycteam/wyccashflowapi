<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AIzaSyA7ev628GjNpVE5WMy7mhARgvFur_JZ-zg',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AIzaSyA7ev628GjNpVE5WMy7mhARgvFur_JZ-zg',
    ],
    'apn' => [
        'certificate' => __DIR__.'/iosCertificates/cashflow.pem',
        'passPhrase' => 'qwerty123', //Optional
        'dry_run' => true,
    ],
];
