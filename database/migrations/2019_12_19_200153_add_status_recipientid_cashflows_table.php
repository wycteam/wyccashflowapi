<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusRecipientidCashflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cashflows',function(Blueprint $table){
            $table->enum('status',["1","0"])->nullable()->default("0")->comment('状态，我也不知道这个到底有什么用。直接就说需要');
            $table->integer('recipientid')->nullable()->comment('转账时,得到被转让的id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
