<?php
use Illuminate\Http\Request;
Route::get('currencies','Api\CashflowController@currencies');
Route::post('check-pending-transaction','Api\CashflowController@checkpendingtransaction');
Route::get('check-pending-transaction','Api\CashflowController@checkpendingtransaction');
Route::middleware('auth:api')->group(function() {
    //我也不知道应用在哪里，只是不想修改数据库。重新写个API。。。
    Route::get('summaries','Api\CashflowController@summaries');
    Route::get('cashflow/{id}','Api\CashflowController@getCurrentUserTransactionData');

    Route::post('verify-password','Api\CashflowController@verifypassword');
    Route::post('transfer','Api\CashflowController@transfer');
    
    
    Route::get('user-info/mobile-number/{number}','Api\CashflowController@number');
});
