<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//login   登录
Route::post('login','Api\LoginController@login');
Route::group(['middleware'=>'auth:api'],function(){
    //退出
    Route::get('logout','Api\LoginController@logout');
    //注册
    Route::post('register','Api\LoginController@register');
    //得到当前的user信息
    Route::post('user-info','Api\UserController@getCurrentUser');
    Route::get('user-info','Api\UserController@getCurrentUser');
    //得到所有的user信息
    Route::post('getAllUserInfo','Api\UserController@getAllUserInfo');
    //得到交易日志
    Route::post('getTheTransactionLog','Api\CashflowController@getTheTransactionLog');
    //得到服务费流水日志
    Route::post('getServiceFeeFlowLog','Api\CashflowController@getServiceFeeFlowLog');
    //添加交易日志
    Route::post('addTheTransactionLog','Api\CashflowController@addTheTransactionLog');
    //修改当前用户的信息
    Route::post('modifyCurrentUserInformation','Api\UserController@modifyCurrentUserInformation');
    //修改个人用户的信息
    Route::post('change-password','Api\UserController@editPersonalInformation');
});

